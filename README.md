## Installation

To build this project, please build images and run them via compose:
```
$ docker-compose build
$ docker-compose up
```

## Run

```
$ docker exec idaproject_idaproject_1 ./manage.py migrate
```

