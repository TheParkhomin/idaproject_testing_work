from django.contrib import admin
from .models import IdaImage
# Register your models here.


@admin.register(IdaImage)
class IdaImageAdmin(admin.ModelAdmin):
    exclude = ["image", "image_link"]
