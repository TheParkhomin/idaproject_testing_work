from django import forms


class IdaProjectImageForm(forms.Form):
    link = forms.URLField()
    image = forms.ImageField()
