import requests
from PIL import Image
from django.conf import settings
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils.translation import ugettext_lazy as _

CURRENT_DIR = "/".join(settings.BASE_DIR.split("/")[0:-1])


def idaproject_source_filename(instance, filename):
    return '/'.join(['images', filename])


def idaproject_filename(instance, filename):
    width, height = instance.width, instance.height
    if not (width and height):
        width, height = instance.image_source.width, instance.image_source.height
    name, format_file = ".".join(filename.split('.')[0:-1]), filename.split('.')[-1]
    return idaproject_source_filename(instance, f"{name}_{width}_{height}.{format_file}")


class IdaImage(models.Model):
    image_source = models.ImageField(
        verbose_name=_("Source image"),
        upload_to=idaproject_source_filename,
        blank=True
    )
    image = models.ImageField(
        verbose_name=_("Image"),
        upload_to=idaproject_filename,
        blank=True
    )
    image_link = models.URLField(
        verbose_name=_("Link to image"),
        blank=True,
        null=True
    )
    height = models.IntegerField(
        verbose_name=_("Height"),
        blank=True,
        null=True
    )
    width = models.IntegerField(
        verbose_name=_("Width"),
        blank=True,
        null=True
    )

    class Meta:
        verbose_name = _("Idaproject image")
        verbose_name_plural = _("Idaproject images")

    def __str__(self):
        return self.image_source.name

    def _get_new_file(self, width, height):
        new_file_path = "{}_{}_{}.{}".format(
            ".".join(self.image_source.path.split(".")[0:-1]),
            width,
            height,
            self.image_source.path.split(".")[-1]
        )
        new_file_name = new_file_path.replace("{}/{}".format(CURRENT_DIR, settings.MEDIA_ROOT), "")
        return new_file_path, new_file_name

    def _thumbnail(self):
        image = Image.open(self.image_source.path).copy()
        image.thumbnail((self.width, self.height))
        self.width, self.height = image.width, image.height

        new_file_path, new_file_name = self._get_new_file(image.width, image.height)
        self.image.name = new_file_name
        image.save(new_file_path)

    def save(self, *args, **kwargs):
        if not self.image_source and self.image_link:
            filename = self.image_link.split("/")[-1][-20:]
            filepath = "{}/{}{}".format(CURRENT_DIR, settings.MEDIA_ROOT, idaproject_source_filename(self, filename))

            r = requests.get(self.image_link, allow_redirects=True)

            with open(filepath, 'wb+') as file:
                file.write(r.content)
            self.image_source.name = idaproject_source_filename(self, filename)

        if self.pk:
            old_instance = self.__class__.objects.get(id=self.pk)
            if (
                    (old_instance.width and old_instance.height) and
                    (
                            (old_instance.width != self.width) or (old_instance.height != self.height)
                    )
            ):
                self._thumbnail()

        super().save(*args, **kwargs)


@receiver(post_save, sender=IdaImage)
def my_handler(sender, **kwargs):
    instance = kwargs["instance"]
    if not instance.image and instance.image_source:
        new_image = Image.open(instance.image_source.path).copy()
        new_file_path, new_file_name = instance._get_new_file(
            instance.image_source.width, instance.image_source.height
        )
        new_image.save(new_file_path)
        instance.image.name = new_file_name
        instance.height = instance.image_source.height
        instance.width = instance.image_source.width
        instance.save()
