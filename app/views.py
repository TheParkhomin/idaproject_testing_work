from django.shortcuts import render, redirect, get_object_or_404
from .models import IdaImage
import requests


def index(request):
    ida_images = IdaImage.objects.all()
    return render(request, "app/index.html", {"ida_images": ida_images})


def add_image(request):
    error = None

    if request.POST:
        image = request.FILES.get("image")
        url = request.POST["link"]
        if image and url:
            error = "Нужно указать только файл или ссылку. Оставьте что-нибудь одно."
        elif not (image or url):
            error = "Укажите файл или ссылку"

        if not error:
            initial_data = {}
            if image:
                initial_data["image_source"] = image
            elif url:
                initial_data["image_link"] = url
            new_ida_images = IdaImage.objects.create(**initial_data)
            return redirect("detail_image", new_ida_images.id)

    return render(request, "app/create_image.html", {"error": error})


def detail_image(request, pk):
    ida_image = get_object_or_404(IdaImage, pk=pk)
    if request.POST:
        ida_image.width = int(request.POST["width"])
        ida_image.height = int(request.POST["height"])
        ida_image.save()
    return render(request, "app/detail_image.html", {"ida_image": ida_image})
