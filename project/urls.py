from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path

from app import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.index, name="list_images"),
    path('add/', views.add_image, name="create_image"),
    path('image/<int:pk>/', views.detail_image, name="detail_image")
    # path('contract/', views.deactivate)
] + static(
    settings.STATIC_URL,
    document_root=settings.STATIC_ROOT
) + static(
    settings.MEDIA_URL,
    document_root=settings.MEDIA_ROOT
)
